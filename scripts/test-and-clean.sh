#!/bin/bash
python setup.py test
rm -rf .eggs
rm -rf *.egg-info
find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

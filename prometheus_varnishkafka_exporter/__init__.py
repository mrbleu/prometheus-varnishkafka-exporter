#!/usr/bin/env python3
# Copyright 2019 Cole White
#                Wikimedia Foundation
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Prometheus VarnishKafka Exporter"""

import argparse
from argparse import Namespace
import logging
import threading
import os
import json
import yaml
from file_read_backwards import FileReadBackwards

from prometheus_client import start_http_server
from prometheus_client.core import (
    REGISTRY,
    CounterMetricFamily,
    GaugeMetricFamily,
    SummaryMetricFamily,
    HistogramMetricFamily,
    Gauge
)

LOG = logging.getLogger(__name__)

# New configuration keys must be set here to be loaded
CONFIG_DEFAULTS = {
    'stats_files': {
        'type': list,
        'description': 'List of files to scrape metrics from',
        'default': []
    },
    'num_entries_to_get': {
        'type': int,
        'description': 'Number of entries from the stats_files to read',
        'default': 2
    },
    'required_entries': {
        'type': list,
        'description': 'List of entry keys that must be read before rendering metrics',
        'default': []
    },
    'stats': {
        'type': dict,
        'description': 'Dict of stats mappings',
        'default': {}
    }
}


class PrometheusVarnishKafkaExporter:
    """Prometheus VarnishKafka Exporter"""

    last_success = Gauge(
        'varnishkafka_exporter_last_success_time',
        'Timestamp of when metrics were last successfully gathered and served',
        []
    )

    def __init__(self, config):
        self.config = config
        self.metrics = []
        self.initial_run = True
        self.successful_run = False

    def get_entries(self, filename: str, count: int,  encoding: str = 'ascii', reader=FileReadBackwards) -> list:
        """Returns the last n (count) entries from the provided file at filename"""
        output = []
        try:
            with reader(filename, encoding) as f:
                for line in f:
                    if len(output) >= count:
                        break
                    output.append(json.loads(line))
        except FileNotFoundError as e:
            LOG.exception(e)
            self.fail_run()
        except json.JSONDecodeError as e:
            LOG.exception(e)
            self.fail_run()
        return output

    def fail_run(self):
        if self.successful_run:
            self.successful_run = False

    def _handle_key_name_branch(self, block: dict, metrics: dict, trail: list) -> None:
        """
        Brokers, topics, and partitions are branches from the main object keyed by name.
        We can ignore the top-level keys and instead look to the name, topic, or partition keys
        in the child object when resolving the labels.
        """
        for bloc in block.values():
            self._populate_metrics(bloc, metrics, trail)

    def _populate_metrics(self, block: dict, metrics: dict, trail: list = None) -> None:
        """
        This function serves three purposes:
        1. Manage the block "trail" so that find_in_trail() has the data to search through
        2. Handle branches for "brokers", "topics", and "partitions"
        3. Send the block down a level with the metrics to be resolved and constructed
        """
        if trail is None:
            trail = []
        trail.append(block)
        for key, metric in metrics.items():
            if key in ['brokers', 'topics', 'partitions']:  # Handle branches
                self._handle_key_name_branch(block[key], metrics[key], trail)
            else:
                if key in block.keys():
                    self._handle_branches(block[key], metric, trail)
        trail.pop()

    def _handle_branches(self, block, metric, trail):
        """Based on the data, we either recurse down a level or start constructing metrics"""
        if isinstance(block, dict):
            self._populate_metrics(block, metric, trail)
        else:
            self._construct_new_metrics(block, metric, trail)

    def _construct_new_metrics(self, block, metric, trail):
        """Construct metrics from block and configured metric data"""
        label_names = ['source']
        if metric.get('labels'):
            label_names += metric['labels']
        label_names, label_values = self.get_labels(trail, label_names)
        try:
            # Get a new instance of the metric based on configured type
            t = globals()[metric['type']](metric['name'], metric['description'], labels=label_names)
            t.add_metric(label_values, block)
            self.metrics.append(t)
        except KeyError:
            self.fail_run()
            LOG.error('Configured type {} not found.  Skipping {}.'.format(metric['type'], metric['name']))

    @staticmethod
    def check_kafka_instance_type(entry: dict):
        if entry.get('type') != 'producer':
            LOG.warning('Kafka instance type {} not supported.'.format(entry['type']))
            raise NotImplementedError

    @staticmethod
    def find_in_trail(key: str, trail: list):
        for idx in range(len(trail), 0, -1):
            idx -= 1
            if key in trail[idx]:
                return trail[idx][key]

    def get_labels(self, trail: list, label_names: list) -> tuple:
        """Attempts to resolve labels by iterating backwards over the trail and looking for the label"""
        output = {}
        for raw in label_names:
            label, search_key = self.extract_label(raw)
            output[label] = self.find_in_trail(search_key, trail)
        for key, value in output.items():
            if value is None:
                LOG.error('Could not resolve label: {}'.format(key))
                self.fail_run()
                output[key] = 'undef'  # default value
        return list(output.keys()), [str(x) for x in output.values()]

    @staticmethod
    def extract_label(string: str) -> tuple:
        if '=' in string:
            return tuple(string.split('='))
        return string, string

    @staticmethod
    def extract_source(filename: str) -> bytes:
        """Extracts source label from filename"""
        head, tail = os.path.split(filename)
        return tail.split('.')[0]

    def load_stats(self) -> None:
        """Reads state from entries and populates metrics"""
        for filename in self.config.stats_files:
            required_entries = {}
            if self.config.required_entries:
                required_entries = {x: False for x in self.config.required_entries}
            for entry in self.get_entries(filename, self.config.num_entries_to_get):
                for key in self.config.stats:
                    if entry.get(key):
                        required_entries[key] = True
                        if key == 'kafka':  # Hack: Only supporting producers in this iteration
                            try:
                                self.check_kafka_instance_type(entry[key])
                            except NotImplementedError:
                                continue
                        self._populate_metrics(
                            entry[key],
                            self.config.stats[key],
                            [{'source': self.extract_source(filename)}]  # root of the trail for label resolving
                        )
            if False in required_entries.values():
                LOG.error('Not all required_entries were found and loaded.')
                self.fail_run()

    def collect(self) -> (
            CounterMetricFamily,
            GaugeMetricFamily,
            SummaryMetricFamily,
            HistogramMetricFamily,
    ):
        """
        The function that prometheus_client expects to get metrics from.

        :return: Metric
        """
        if self.initial_run:
            # Prometheus client runs collect() on register() and the
            # application will fail to start if an error like ENOENT occurs at startup.
            # Returning None on initial run will give it the chance to at least start
            # and do the right thing if the metrics file appears later.
            self.last_success.set_to_current_time()
            self.initial_run = False
            return
        self.successful_run = True
        self.metrics = []
        self.load_stats()
        if self.successful_run:
            self.last_success.set_to_current_time()
        for metric in self.metrics:
            yield metric


def validate_config(config) -> None:
    """Configuration validation rules"""
    for key, default_config in CONFIG_DEFAULTS.items():
        if not isinstance(getattr(config, key), default_config['type']):
            raise TypeError('"{}" is not the correct type.  Expected {}'.format(key, default_config['type'].__name__))
    if config.num_entries_to_get < 1:
        raise ValueError(
            '"num_entries_to_get" must be >= 1'
        )
    if len(config.required_entries) > config.num_entries_to_get:
        raise ValueError(
            'Count of "required_entries" ({}) cannot be larger than "num_entries_to_get" ({}).'
            ' Check the config.'.format(len(config.required_entries), config.num_entries_to_get)
        )


def get_config(filename: str) -> Namespace:
    """Load the config from filename, assign defaults where not found, and run validation"""
    ns = Namespace()
    with open(filename, 'r') as f:
        parsed = yaml.safe_load(f.read())

    if not parsed:  # A present but empty yaml file renders a NoneType
        raise RuntimeError('Config file appears to be empty.  Check the config.')

    for key, default_config in CONFIG_DEFAULTS.items():
        value = parsed.get(key)
        if value is None:
            LOG.warning('Could not find "{}". Using default value'.format(key))
            value = default_config['default']
        setattr(ns, key, value)
    validate_config(ns)
    return ns


def print_config_options():
    """Prints configuration options from CONFIG_DEFAULTS"""
    for key, config in CONFIG_DEFAULTS.items():
        print(
            '{} [{}]: {}.  Default: "{}"'.format(
                key,
                config['type'].__name__,
                config['description'],
                config['default']
            )
        )


def main() -> int:
    """ Main """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l",
        "--listen",
        metavar="ADDRESS",
        help="Listen on this address",
        default="0.0.0.0:9132",
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="Enable debug logging"
    )
    parser.add_argument(
        "-c",
        "--configfile",
        metavar="FILE",
        help="Config file in yaml format",
        default='example.config.yaml',
    )
    parser.add_argument(
        "--show-config-options",
        help="Prints configuration options and exits.",
        action='store_true'
    )
    args = parser.parse_args()

    if args.show_config_options:
        print_config_options()
        return 0

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARNING)

    address, port = args.listen.split(":", 1)

    LOG.info("Starting varnishkafka exporter on %s:%s", address, port)

    REGISTRY.register(PrometheusVarnishKafkaExporter(get_config(args.configfile)))
    start_http_server(int(port), addr=address)
    # In lieu of a infinite loop, join the thread started by exposition.start_http_server
    try:
        threading.enumerate()[-1].join()
    except KeyboardInterrupt:
        return 1
    return 0


if __name__ == "__main__":
    exit(main())
